var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var request = require('request');
var http = require('http');
var q = require('q');
var cheerio = require('cheerio');
var colors = require('colors');

mongoose.connect('mongodb://t1mg:coupoen4930@ds045598.mongolab.com:45598/yc');

var entrySchema = new Schema({
    title: String,
    id: Number,
    position: Number
})
var snapshotSchema = new Schema({
    ts: String,
    items: [entrySchema]
});

var Snapshot = mongoose.model('Snapshot', snapshotSchema);


// call api
// get previous set
// calculate delta
// save


/**
 * getSnapshot()
 *
 * Requests news.ycombinator.com page and
 * parses it with Cheerio to find needed DOM nodes
 *
 * @returns promise
 * @resolves {items: items}
 */
var getSnapshot = function () {
    var deferred = q.defer(),
        items = [],
        item = {},
        TRs = [],
        count = 0,
        length = 90; // There are 30 items, each uses 3 <tr>'s

    request({
        uri: 'http://news.ycombinator.com',
        json: true
    }, function(error, response, body) {
        if (error) {
            deferred.reject(error);
            return;
        }
        $ = cheerio.load(body);

        if (!$('table').length || !$('table')[2] || !$('table')[2].children) {
            deferred.reject(new Error("getSnapshot: No valid data returned"));
            return;
        }

        TRs = $('table')[2].children;

        /* Iterating over first <length> TRs */
        while (count < length) {
            item = {};

            /* position */
            item.position = count/3 + 1;

            /* id: */
            try {
                item.id = TRs[count].children[1].children[0].children[0].attribs.id.substring(3);
            } catch (e) {
                console.warn("No id on item ".yellow + item.position);
                item.id = "";
            }

            /* title */
            item.title = TRs[count].children[2].children[0].children[0].data;

            /* href */
            item.href = TRs[count].children[2].children[0].attribs.href || "";

            count ++;

            /* points */
            try {
                item.points = parseInt(TRs[count].children[1].children[0].children[0].data.match(/^([\w\-]+)/)[0]) || "0";
            } catch (e) {
                console.warn("No points on item ".yellow + item.position);
                item.points = 0;
            }

            count ++;
            count ++;

            if (item.id) items.push(item);
        }

        console.log("getSnapshot: New data received".green);
        deferred.resolve({items: items});
    });
    return deferred.promise;
},


    /**
     * getPreviousSnapshot()
     *
     * Get latest snapshot from Mongolab
     *
     * @returns promise
     * @resolve snapshot
     **/
    getPreviousSnapshot = function () {
        var deferred = q.defer();
        Snapshot.find().sort({ts: -1}).limit(1).find(function (error, doc) {
            if (error) {
                deferred.reject(error);
                return;
            }
            console.log("getPreviousSnapshot: Old snapshot received".green);
            deferred.resolve(doc[0]);
        });
        return deferred.promise;
    },


    /**
     * processData()
     *
     * Prepares new snapshot to store from 2 pieces of data:
     *
     * @param snapshot - current list of items
     * @param previousSnapshot - last snapshot
     */
    processData = function (snapshot, previousSnapshot) {
        var count = 0,
            entries= [],
            entry = {};

        console.log("processData() started".green);

        snapshot.items.forEach(function (snapshotItem) {
            entry = {};
            count ++;

            entry.title = snapshotItem.title;
            entry.id = snapshotItem.id;
            entry.position = count;
            entry.delta = getDelta(snapshotItem, previousSnapshot.items);

            entries.push(entry);
        });

        var newSnapshot = new Snapshot({
            ts: new Date().getTime(),
            items: entries
        });

        newSnapshot.save(function (error) {
            if (error) {
                console.error(error);
            } else {
                console.log("Saved snapshot " + newSnapshot.ts + " with " + count + " entries");
            }
            process.exit();
        });
    },


    /**
     * getDelta()
     *
     * Calculates a delta of an entry
     *
     * @params currentItem entry to compare
     * @params previousItems all previous entries
     */
    getDelta = function (currentItem, previousItems) {
        var match = null;

        previousItems.forEach(function(item){
           if (item.id == currentItem.id) {
               match = item;
           }
        });

        delta = match ? currentItem.position - match.position : "new";

        return delta;
    };



var main = function () {
    var promises = [];

    promises.push(getSnapshot());
    promises.push(getPreviousSnapshot());

    q.all(promises).then(function(data){
        console.log("Both promises resolved, calling process()".green)
        processData(data[0], data[1]);
    }, function (error) {
        console.error(error);
        process.exit();
    });
};

main();

